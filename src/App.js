import { useEffect, useState } from "react";
import "./App.css";

function App() {
  const [isSorted, setIsSorted] = useState(false);
  const [searchText, setSearchText] = useState("");
  const [listData, setListData] = useState([
    {
      Product: "Sleeve",
      Inventory: 155,
      Type: "Toy",
      Vendor: "Bella",
    },
    {
      Product: "Sock",
      Inventory: 55,
      Type: "Women",
      Vendor: "Alle",
    },
    {
      Product: "Testing",
      Inventory: 522,
      Type: "Wumbo",
      Vendor: "Gumbo",
    },
    {
      Product: "Playstation 5",
      Inventory: 1,
      Type: "Electronics",
      Vendor: "Sony",
    },
    {
      Product: "Hot Coffee Decaf",
      Inventory: 522,
      Type: "Beverage",
      Vendor: "Dunkin",
    },
  ]);
  const [tempList, setTempList] = useState(listData);

  let handleInputChange = (e) => {
    if (e.target.value === "") {
      setTempList(listData);
    } else {
      setTempList(listData.filter((x) => x.Product.match(e.target.value) || x.Type.match(e.target.value) || x.Vendor.match(e.target.value)));
    }
  };

  let orderedColumns = () => {

    let testingList = tempList.sort((a, b) => {
      let pa, pb = null;

        pa = a.Product.toLowerCase();
        pb = b.Product.toLowerCase();
      
      
      if (!isSorted) {
        setIsSorted(!isSorted);
        if (pa < pb) return -1;

        if (pa > pb) return 1;

        return 0;
      } else {
        setIsSorted(!isSorted);
        if (pa > pb) return -1;

        if (pa < pb) return 1;

        return 0;
      }
    });
    setTempList(testingList)

  }

  return (
    <div className="App">
      <table id="react_table">
        <thead>
          <tr>
            <input name="uniqueD" onChange={handleInputChange}></input>
          </tr>
          <tr>
            <th onClick={orderedColumns}>Product</th>
            <th>Inventory</th>
            <th>Type</th>
            <th>Vendor</th>
          </tr>
        </thead>
        <tbody>
          {/* <tr>
            <td>Sleevless Shirt</td>
            <td>10</td>
            <td>Workout</td>
            <td>Bally</td>
          </tr>
          <tr>
            <td>Workout Belt</td>
            <td>43</td>
            <td>Workout</td>
            <td>ArmorAll</td>
          </tr>
          <tr>
            <td>Orange Teal</td>
            <td>100</td>
            <td>Men</td>
            <td>Definitive</td>
          </tr> */}
          {tempList.map((x) => {
            return (
              <tr>
                <td>{x.Product}</td>
                <td>{x.Inventory}</td>
                <td>{x.Type}</td>
                <td>{x.Vendor}</td>
                <td>
                  <button>Delete</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default App;
